To get more detailed information about some the contents of the requested/returned
representation of resources related to users, see also :class:`.User` and the
:class:`.Identity` classes.
