`Elasticsearch <https://www.elastic.co/elasticsearch>`__ is the full-text search engine
used in the application. Currently, only version **8** is supported, which can be
installed like this:

.. code-block:: bash

    sudo apt install wget apt-transport-https gnupg
    wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo gpg --dearmor -o /usr/share/keyrings/elasticsearch-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/elasticsearch-keyring.gpg] https://artifacts.elastic.co/packages/8.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-8.x.list
    sudo apt update && sudo apt install elasticsearch

Some settings have to be changed after installation by using the following command,
which will overwrite the default configuration file. Among others, these settings
configure Elasticsearch to use a single-node cluster and disable the basic security
features included in free Elasticsearch installations, which are not needed in this
simple setup.

.. code-block:: bash

    echo -e "path.data: /var/lib/elasticsearch\npath.logs: /var/log/elasticsearch\ndiscovery.type: single-node\nxpack.security.enabled: false\nxpack.security.transport.ssl.enabled: false\nxpack.security.http.ssl.enabled: false" | sudo tee /etc/elasticsearch/elasticsearch.yml

To start Elasticsearch and also configure it to start automatically when the system
boots, the following commands can be used:

.. code-block:: bash

    sudo systemctl enable elasticsearch.service
    sudo systemctl start elasticsearch.service
