# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.config.core import set_user_config
from kadi.lib.favorites.core import toggle_favorite
from kadi.lib.permissions.core import add_role
from kadi.lib.permissions.core import set_system_role
from kadi.lib.permissions.models import Role
from kadi.modules.accounts.core import merge_users
from kadi.modules.accounts.core import purge_user
from kadi.modules.accounts.models import User
from kadi.modules.records.core import update_record


def test_purge_user(
    dummy_collection,
    dummy_file,
    dummy_group,
    dummy_oauth2_server_client,
    dummy_personal_token,
    dummy_record,
    dummy_template,
    dummy_upload,
    dummy_user,
    new_record,
    new_user,
):
    """Test if purging users works correctly."""
    user = new_user()
    record = new_record(creator=user)

    merged_user_1 = new_user()
    merged_user_2 = new_user()
    merged_user_3 = new_user()

    # Merge several users into the dummy user to ensure they get deleted correctly as
    # well.
    merge_users(merged_user_1, merged_user_2)
    merge_users(merged_user_1, merged_user_3)
    merge_users(dummy_user, merged_user_1)

    # Make the dummy user update a record that was created by another user to verify
    # that the corresponding revision won't prevent the user from being deleted.
    update_record(record, description="test", user=dummy_user)

    purge_user(dummy_user)

    # Only the newly created user should remain.
    assert User.query.one() == user
    # The revision the dummy user made should remain.
    assert record.revisions.count() == 2


def test_merge_users(new_record, new_user):
    """Test if merging two users works correctly."""
    primary_user = new_user()
    record_1 = new_record(creator=primary_user)
    set_system_role(primary_user, "admin")
    set_user_config("TEST1", "primary", user=primary_user)
    toggle_favorite("record", record_1.id, user=primary_user)

    secondary_user = new_user()
    record_2 = new_record(creator=secondary_user)
    set_user_config("TEST1", "secondary", user=secondary_user)
    set_user_config("TEST2", "secondary", user=secondary_user)
    toggle_favorite("record", record_1.id, user=secondary_user)
    toggle_favorite("record", record_2.id, user=secondary_user)

    add_role(primary_user, "record", record_2.id, "member")
    add_role(secondary_user, "record", record_1.id, "member")

    merge_users(primary_user, secondary_user)

    assert not primary_user.is_merged
    assert primary_user.identity
    assert primary_user.identities.count() == 2
    assert primary_user.records.count() == 2
    assert primary_user.config_items.count() == 2
    assert primary_user.config_items.filter_by(key="TEST1").first().value == "primary"
    assert primary_user.favorites.count() == 2

    # Only one system role should remain.
    assert primary_user.roles.filter(Role.object.is_(None)).one().name == "admin"

    # Only the admin roles from both users should remain.
    record_roles = primary_user.roles.filter(Role.object == "record")

    assert record_roles.count() == 2

    for record_role in record_roles:
        assert record_role.name == "admin"

    assert secondary_user.is_merged
    assert secondary_user.new_user_id == primary_user.id
    assert not secondary_user.identity
    assert not secondary_user.identities.all()
    assert not secondary_user.records.all()
    assert not secondary_user.config_items.all()
    assert not secondary_user.favorites.all()
    assert not secondary_user.roles.all()
