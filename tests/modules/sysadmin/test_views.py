# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import get_flashed_messages

import kadi.lib.constants as const
from kadi.lib.config.core import get_sys_config
from kadi.lib.web import url_for
from kadi.modules.accounts.models import LocalIdentity
from tests.utils import check_view_response


def test_view_information(client, dummy_user, user_session):
    """Test the "sysadmin.view_information" endpoint."""
    dummy_user.is_sysadmin = True

    with user_session():
        response = client.get(url_for("sysadmin.view_information"))
        check_view_response(response)


def test_manage_config_customization(client, dummy_user, user_session):
    """Test the "customization" tab of the "sysadmin.manage_config" endpoint."""
    dummy_user.is_sysadmin = True
    endpoint = url_for("sysadmin.manage_config", tab="customization")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"broadcast_message": "Test"})

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert (
            get_sys_config(const.SYS_CONFIG_BROADCAST_MESSAGE, use_fallback=False)
            == "Test"
        )


def test_manage_config_legals(client, dummy_user, user_session):
    """Test the "legals" tab of the "sysadmin.manage_config" endpoint."""
    dummy_user.is_sysadmin = True
    endpoint = url_for("sysadmin.manage_config", tab="legals")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"terms_of_use": "Test"})

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert (
            get_sys_config(const.SYS_CONFIG_TERMS_OF_USE, use_fallback=False) == "Test"
        )


def test_manage_config_misc(client, dummy_user, user_session):
    """Test the "misc" tab of the "sysadmin.manage_config" endpoint."""
    dummy_user.is_sysadmin = True
    endpoint = url_for("sysadmin.manage_config", tab="misc")

    with user_session():
        response = client.post(
            url_for("sysadmin.manage_config", tab="misc", action="test_email")
        )

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert "A test email has been sent." in get_flashed_messages()

        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"robots_noindex": "true"})

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert (
            get_sys_config(const.SYS_CONFIG_ROBOTS_NOINDEX, use_fallback=False) is True
        )


def test_manage_users_manage(client, dummy_user, user_session):
    """Test the "manage" tab of the "sysadmin.manage_users" endpoint."""
    dummy_user.is_sysadmin = True
    endpoint = url_for("sysadmin.manage_users", tab="manage")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)


def test_manage_users_merge(client, dummy_user, new_user, user_session):
    """Test the "merge" tab of the "sysadmin.manage_users" endpoint."""
    dummy_user.is_sysadmin = True
    secondary_user = new_user()
    endpoint = url_for("sysadmin.manage_users", tab="merge")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={"primary_user": dummy_user.id, "secondary_user": secondary_user.id},
        )

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert secondary_user.is_merged


def test_manage_users_register(client, dummy_user, user_session):
    """Test the "register" tab of the "sysadmin.manage_users" endpoint."""
    dummy_user.is_sysadmin = True
    endpoint = url_for("sysadmin.manage_users", tab="register")
    new_username = "test"

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={
                "displayname": new_username,
                "username": new_username,
                "email": "test@test.com",
            },
        )

        check_view_response(response)
        assert LocalIdentity.query.filter_by(username=new_username).one()
