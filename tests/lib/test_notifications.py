# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from uuid import uuid4

import pytest

from kadi.lib.notifications.core import create_notification_data
from kadi.lib.notifications.core import dismiss_notification
from kadi.lib.notifications.models import Notification
from kadi.lib.notifications.models import NotificationNames
from kadi.lib.tasks.models import Task
from kadi.lib.tasks.models import TaskState


@pytest.mark.parametrize(
    "name,title,body",
    [
        ("test", "test", "test"),
        (NotificationNames.TASK_STATUS, "Task status", "Task no longer exists."),
    ],
)
def test_create_notification_data(name, title, body, dummy_user):
    """Test if creating notification data works correctly."""
    notification = Notification.create(
        user=dummy_user, name=name, data={"task_id": str(uuid4())}
    )
    assert create_notification_data(notification) == (title, body)


def test_dismiss_notification(db, dummy_user):
    """Test if dismissing notifications works correctly."""
    task = Task.create(creator=dummy_user, name="test", state=TaskState.PENDING)
    db.session.commit()

    notification = Notification.create(
        user=dummy_user,
        name=NotificationNames.TASK_STATUS,
        data={"task_id": str(task.id)},
    )
    db.session.commit()

    dismiss_notification(notification)
    db.session.commit()

    assert not Notification.query.all()
    assert task.state == TaskState.REVOKED
