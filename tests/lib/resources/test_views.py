# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.permissions.models import Role
from kadi.lib.resources.views import add_links
from kadi.lib.resources.views import update_roles
from kadi.modules.collections.models import Collection
from kadi.modules.collections.models import CollectionState


def test_add_links(dummy_record, dummy_user, new_collection, new_user):
    """Test if adding links in view functions works correctly."""
    valid_collection = new_collection()
    invalid_collection_ids = [
        new_collection(creator=new_user()).id,
        new_collection(state=CollectionState.DELETED).id,
    ]

    add_links(
        Collection,
        dummy_record.collections,
        [valid_collection.id, *invalid_collection_ids, invalid_collection_ids[-1] + 1],
        user=dummy_user,
    )

    assert dummy_record.collections.one().id == valid_collection.id


def test_update_roles(dummy_group, dummy_record, dummy_user, new_group, new_user):
    """Test if updating roles in view functions works correctly."""

    # Try adding a new user role.
    user = new_user()

    update_roles(
        dummy_record,
        [{"subject_type": "user", "subject_id": user.id, "role": "member"}],
        user=dummy_user,
    )
    assert user.roles.filter(Role.object == "record").one().name == "member"

    # Try removing the role again.
    update_roles(
        dummy_record,
        [{"subject_type": "user", "subject_id": user.id, "role": None}],
        user=dummy_user,
    )
    assert not user.roles.filter(Role.object == "record").all()

    # Try changing the role of the resource creator.
    update_roles(
        dummy_record,
        [{"subject_type": "user", "subject_id": dummy_user.id, "role": "member"}],
        user=dummy_user,
    )
    assert dummy_user.roles.filter(Role.object == "record").one().name == "admin"

    # Try adding a new group role.
    group = new_group()

    update_roles(
        dummy_record,
        [{"subject_type": "group", "subject_id": group.id, "role": "member"}],
        user=dummy_user,
    )
    assert group.roles.filter(Role.object == "record").one().name == "member"

    # Try adding a new group role to another group.
    update_roles(
        dummy_group,
        [{"subject_type": "group", "subject_id": group.id, "role": "member"}],
        user=dummy_user,
    )
    assert not group.roles.filter(Role.object == "group").all()

    # Try adding a new group role to a group not readable by the dummy user.
    group = new_group(creator=user)

    update_roles(
        dummy_record,
        [{"subject_type": "group", "subject_id": group.id, "role": "member"}],
        user=dummy_user,
    )
    assert not group.roles.filter(Role.object == "record").all()
